# Define the Azure Resource Group
resource "azurerm_resource_group" "k8s" {
  name     = var.resource_group
  location = var.cluster_zone
}
