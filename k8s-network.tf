# Criar a virtual private cloud com 3 subnets publicas e 3 subnets privadas
resource "azurerm_virtual_network" "k8s_vpc" {
  name                = "k8s_vpc"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.k8s.location
  resource_group_name = azurerm_resource_group.k8s.name
}


resource "azurerm_subnet" "k8s_public_subnet" {
    count                   = 3
    name           = "k8s_public_subnet_${count.index + 1}"
    address_prefixes = ["10.0.${count.index}.0/24"]
	resource_group_name = azurerm_resource_group.k8s.name
	virtual_network_name = azurerm_virtual_network.k8s_vpc.name
	service_endpoints    = ["Microsoft.ContainerRegistry", "Microsoft.EventHub", "Microsoft.KeyVault", "Microsoft.Sql", "Microsoft.Storage", "Microsoft.AzureCosmosDB"]
}


resource "azurerm_subnet" "k8s_private_subnet" {
    count                   = 3
    name           = "k8s_private_subnet_${count.index + 1}"
    address_prefixes = ["10.0.${count.index + 3}.0/24"]
	resource_group_name = azurerm_resource_group.k8s.name
	virtual_network_name = azurerm_virtual_network.k8s_vpc.name		
	service_endpoints    = ["Microsoft.ContainerRegistry", "Microsoft.EventHub", "Microsoft.KeyVault", "Microsoft.Sql", "Microsoft.Storage", "Microsoft.AzureCosmosDB"]
}



# cria um security group para as subnets publicas
resource "azurerm_network_security_group" "k8s_public_nsg" {
  name                = "k8s_public_nsg"
  location            = azurerm_resource_group.k8s.location
  resource_group_name = azurerm_resource_group.k8s.name
}

# define regra para permitir tráfego da internet as redes publicas em portas especificas
resource "azurerm_network_security_rule" "k8s_public_allow" {
  name                        = "k8s-allow-internet"
  priority                    = 1001
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = 443
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.k8s.name
  network_security_group_name = azurerm_network_security_group.k8s_public_nsg.name
}

# cria um security group para as subnets privadas
resource "azurerm_network_security_group" "k8s_private_nsg" {
  name                = "k8s_private_nsg"
  location            = azurerm_resource_group.k8s.location
  resource_group_name = azurerm_resource_group.k8s.name
}

# defirne regra para subnet privada
resource "azurerm_network_security_rule" "k8s_private_deny" {
  name                        = "k8s-deny-inbound"
  priority                    = 1001
  direction                   = "Inbound"
  access                      = "Deny"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.k8s.name
  network_security_group_name = azurerm_network_security_group.k8s_private_nsg.name
}

# associa as regras com as subnets
resource "azurerm_subnet_network_security_group_association" "public_subnet_nsg" {
  subnet_id                 = azurerm_subnet.k8s_public_subnet[0].id
  network_security_group_id = azurerm_network_security_group.k8s_public_nsg.id
}

resource "azurerm_subnet_network_security_group_association" "private_subnet_nsg" {
  subnet_id                 = azurerm_subnet.k8s_private_subnet[0].id
  network_security_group_id = azurerm_network_security_group.k8s_private_nsg.id
}
