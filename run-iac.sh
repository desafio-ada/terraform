#!/bin/bash
# Desafio Ada 
# - instala dependencias: azurecli, terraform, kubectl
# - corre o terraform para criar a infraestrutura
# - aplica os deployments no servidor aks 
# https://gitlab.com/desafio-ada/
# thiagomelobr@hotmail.com.
#
# Verifica se o Azure CLI está instalado
if ! command -v az &> /dev/null; then
    echo "O Azure CLI não está instalado. Por favor, faça o download e instale-o."
    echo ">> curl -L https://aka.ms/InstallAzureCli | bash"
    exit 1
fi

# Verifica se o Terraform está instalado
if ! command -v terraform &> /dev/null; then
    echo "O Terraform não está instalado. Por favor, faça o download e instale-o."
    echo ">> https://developer.hashicorp.com/terraform/downloads"
    exit 1
fi

# Verifica se o kubectl está instalado
if ! command -v kubectl &> /dev/null; then
    echo "kubectl não está instalado. Por favor, faça o download e instale-o."
    echo ">> curl -LO \"https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl\""
    exit 1
fi

# Faz login no Azure CLI
az login

# Configura o kubectl usando o Azure CLI
az aks get-credentials --resource-group k8s --name k8s_cluster 

# Clona o repositório Git
git clone https://gitlab.com/desafio-ada/terraform.git

# Entra no diretório do Terraform
cd terraform

# Solicita o ID da assinatura e do locatário
read -p "Digite o ID da Assinatura do Azure: " subscription_id
read -p "Digite o ID do Locatário do Azure: " tenant_id

# Cria o arquivo credentials.tf com ID da assinatura e do locatário
cat <<EOF > credentials.tf
variable "subscription_id" {
  type    = string
  default = "$subscription_id"
}

variable "tenant_id" {
  type    = string
  default = "$tenant_id"
}
EOF

# Inicializa o Terraform
terraform init

# Executa o plano do Terraform
terraform plan

# Aplica a configuração do Terraform
terraform apply -auto-approve

# Entra no diretório do backend
cd ..

# Clona o repositório Git do backend
git clone https://gitlab.com/desafio-ada/backend.git

# Entra no diretório do backend
cd backend

# Aplica os recursos do Kubernetes
kubectl apply -f backend-namespace.yaml
kubectl apply -f backend-deployment.yaml

# Entra no diretório do frontend
cd ..

# Clona o repositório Git do frontend
git clone https://gitlab.com/desafio-ada/frontend.git

# Entra no diretório do frontend
cd frontend

# Aplica os recursos do Kubernetes
kubectl apply -f frontend-namespace.yaml
kubectl apply -f frontend-deployment.yaml

echo "Script concluído com sucesso."
