resource "azurerm_kubernetes_cluster" "k8s_cluster" {
  name                = "k8s_cluster"
  location            = azurerm_resource_group.k8s.location
  resource_group_name = azurerm_resource_group.k8s.name
  dns_prefix          = "k8sdns"

  default_node_pool {
    name       = "ak8spool"
    node_count = 1
    vm_size    = "Standard_B4MS"
	vnet_subnet_id        = azurerm_subnet.k8s_private_subnet[0].id
  }  
    
  identity {
    type = "SystemAssigned"
  }

  network_profile {
    network_plugin     = "azure"
    load_balancer_sku  = "standard"
    load_balancer_profile {
      managed_outbound_ip_count = 1
    }    
    service_cidr   = "10.1.0.0/16"
    dns_service_ip = "10.1.0.10"
	outbound_type      = "loadBalancer"
	
  }
  private_cluster_enabled = true
  sku_tier                = "Standard"
  
}