resource "azurerm_mysql_server" "ada_db_server" {
  name                = "adak8sdbserver"
  location            = azurerm_resource_group.k8s.location
  resource_group_name = azurerm_resource_group.k8s.name
  version      = "8.0"
  administrator_login          = "mysqladmin"
  administrator_login_password = "47vr@xcm}MveHKj{{t"
  sku_name           = "GP_Gen5_2"
  storage_mb         = 51200
  auto_grow_enabled                 = true
  backup_retention_days             = 7
  geo_redundant_backup_enabled      = false
  infrastructure_encryption_enabled = false
  public_network_access_enabled     = true
  ssl_enforcement_enabled           = true
  ssl_minimal_tls_version_enforced  = "TLS1_2"
}

resource "azurerm_mysql_database" "ada_db" {
  name                = "ada_db"
  resource_group_name = azurerm_resource_group.k8s.name
  server_name         = azurerm_mysql_server.ada_db_server.name
  charset             = "utf8"
  collation           = "utf8_general_ci"
}