# terraform
# https://gitlab.com/desafio-ada/
# thiagomelobr@hotmail.com
Codigo Terreform para criar a rede, banco de dados e cluster k8s (aks) no Azure

## Configuração 

variables.tf - configurar zona, resource group, workers.
credentials.tf - credenciais do azure

## Requerimentos 

- [terraform]
- [azure cli] 

## Execução completa infra e app 
Executar o script ./run-iac.sh

## Somente infra
- terraform plan 
- terraform apply
- terraform destroy
