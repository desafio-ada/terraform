variable "cluster_zone" {
  type    = string
  default = "France Central"
}

variable "resource_group" {
  type    = string
  default = "k8s"
}

variable "workers" {
  type    = number
  default = 1
}
